#include "reversed_distribution.h"
#include "rand_helper.h"


int random_value_by_reversed_distribution(const float probability_distribution[],
                                          const int probability_distribution_tab_size,
                                          const int distribution_values[]) {
    float random_value = rand_from_range(0, 1);

    float cumulative_distribution = 0;
    for (int i = 0; i < probability_distribution_tab_size - 1; i++) {
        cumulative_distribution += probability_distribution[i];

        if (cumulative_distribution >= random_value) {
            return distribution_values[i];
        }
    }
    return distribution_values[probability_distribution_tab_size - 1];
}