#include <stdio.h>
#include <stdlib.h>
#include "reversed_distribution.h"
#include "elimination.h"


float probability_distribution(float value) {
    return 3 * value - 15;
}

int main() {
    {
        const float probability_distribution[] = {0.2f, 0.4f, 0.3f, 0.1f};
        const int distribution_values[] = {1, 2, 3, 4};

        const int probability_distribution_tab_size =
                sizeof(probability_distribution) / sizeof(*probability_distribution);

        int random_value;
        int repetitions = 10000;

        while(repetitions--) {
            random_value = random_value_by_reversed_distribution(probability_distribution,
                                                                 probability_distribution_tab_size,
                                                                 distribution_values);
            printf("%d\n", random_value);
        }
    }
    {
        const float x_lower_bound = 30;
        const float x_upper_bound = 100;
        const float probability_value_range = 50;

        int repetitions = 10000;

        while(repetitions--) {
            point *p = random_point_by_elimination(x_lower_bound, x_upper_bound, probability_value_range,
                                                   &probability_distribution);
            if (p != NULL) {
                printf("%f, %f\n", p->x, p->y);

                free(p);
            }
        }
    }
    return 0;
}
