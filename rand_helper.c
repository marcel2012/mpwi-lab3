#include <stdlib.h>
#include "rand_helper.h"


float rand_from_range(float begin, float end) {
    float random_value = (float) rand() / RAND_MAX;
    return random_value * (end - begin) + begin;
}
