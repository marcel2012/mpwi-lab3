#include <stdlib.h>
#include "elimination.h"
#include "rand_helper.h"


point *
random_point_by_elimination(const float x_lower_bound, const float x_upper_bound, const float probability_value_range,
                            float (*probability_distribution)(float value)) {
    const float u1 = rand_from_range(x_lower_bound, x_upper_bound);
    const float u2 = rand_from_range(0, probability_value_range);

    const float distribution = probability_distribution(u1);

    if (u2 <= distribution) {
        point *p = malloc(sizeof(point));
        p->x = u1;
        p->y = distribution;

        return p;
    } else {
        return NULL;
    }
}
