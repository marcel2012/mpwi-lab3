#ifndef ELIMINATION_H
#define ELIMINATION_H


typedef struct point point;
struct point {
    float x, y;
};

point *
random_point_by_elimination(const float x_lower_bound, const float x_upper_bound, const float probability_value_range,
                            float (*probability_distribution)(float value));

#endif
